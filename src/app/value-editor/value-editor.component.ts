import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-value-editor',
  templateUrl: './value-editor.component.html',
  styleUrls: ['./value-editor.component.css']
})
export class ValueEditorComponent implements OnInit {
  public params = {
    s1: 654,
    s2: 12,
    s3: 7,
    s4: 70,
    s5: 12,
    h: 3.4,
    N1: 450,
    n1: 2,
    n2: 2,
    n3: 4,
    N2: 8
  }
  public results = []

  constructor() { }

  calculate(){
    let S, Vp = 1.5, Qosn1 = 11, Qosn2 = 2, Qupr2 = 5, Fvk1 = 1200, Fvk2 = 300, Erv2min = 900, Erv2max = 1300;
    let NormP;
    S = ( this.params.h < 2.9)?0.5:0.4;
    switch (this.params.n3) {
      case 1:
        NormP = 8;
        break;
      case 2:
        NormP = 10;
        break;
      case 3:
        NormP = 11;
        break;
      case 4:
        NormP = 13;
        break;
      default:
        break;
    }
    let Smain = this.params.s1 + this.params.s2;
    let Sall = Smain + this.params.s3 + this.params.s4 + this.params.s5;
    let Ms = Smain/S;
    let Mv = (Sall*this.params.h)/Vp;
    let Mfact = (Ms>Mv)?Mv:Ms;
    let upgrade = '';
    if(Mfact < this.params.N1){
      upgrade = `${Mfact} < ${this.params.N1} , need to upgrade.`
    }
    let mode1  = this.params.n1 * Fvk1;
    let mode2min = this.params.n1 * Fvk2 + this.params.n2 * Erv2min;
    let mode2max = this.params.n1 * Fvk2 + this.params.n2 * Erv2max;
    let Qpotr1 = this.params.N1 * NormP;
    let Qpotr2 = this.params.N1 * Qosn2 + this.params.N2 *Qupr2;
    
    let ndvo = Math.floor((Qpotr1 - mode1)/Fvk1)+1
    this.results = [`1.	Визначила загальну площу основних приміщень, м2: ${Smain};\n`,
  `2.	Визначила загальну площу всіх приміщень у зоні герметизації, м2: ${Sall};\n`,
  `3.	Визначити місткість сховища при відповідному розташуванні ліжок, осіб: ${Ms};\n`,
  `4. Визначити місткість сховища за об’ємом всіх приміщень в зоні герметизації, осіб: ${Mv};\n`,
  `5.	Зробити висновок про можливість укриття в існуючому сховищі найбільшої зміни на об’єкті та необхідність модернізації сховища: ${upgrade}; \n`,
  `6.	Оцінила кількість повітря Q, що подається у сховище: Режим І - ${mode1}, Режим ІІ - [${mode2min};${mode2max}]; \n `,
  `7. Визначити потрібну кількість повітря Qпотр, що подається в приміщення в обох режимах для фактичної місткості сховища та відповідної кліматичної зони: Режим І - ${Qpotr1}, Режим ІІ - ${Qpotr2}; \n`,
  `8.	Зробити висновок про необхідність встановлення додаткового вентиляційного обладнання: Необхідно становити додаткове вентиляційне обладнання в режимі І, в кількості ${ndvo} штук ФВК-І.`
  ]
  }

  ngOnInit() {
  }

}
